// Curry
def summation(f: Int => Int)(start: Int, end: Int): Int = {
  if (start > end) 0
  else f(start) + summation(f)(start + 1, end)
}


// Curry with inner tail recursion
def summationTail(f: Int => Int)(start: Int, end: Int): Int = {
  def nestFunction(x: Int, acc: Int): Int = {
    if (x > end) acc
    else nestFunction(x + 1, f(x) + acc)
  }
  nestFunction(start, 0)
}

def factorial(x: Int): Int = if (x == 0) 1 else x * factorial(x - 1)

def sumNumber = summation(x => x) _
def sumCubes: (Int, Int) => Int = summation(x => x * x * x)
def sumFactorials = summation(factorial) _

sumNumber(1, 3)
sumCubes(1, 3)
sumFactorials(1, 3)