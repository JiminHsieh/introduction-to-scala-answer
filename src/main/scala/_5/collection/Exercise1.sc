// only use collection's API

// find the maximum number of Seq with fun
def larges(f: Int => Int,
           inputs: Seq[Int]): Int = {
  inputs.map(f).max
}

// find the maximum number of index of Seq with fun
def largesIndex(g: Int => Int,
                inputs: Seq[Int]): Int = {
  inputs.map(g).zipWithIndex.maxBy(_._1)._2
}

larges(x => 10 * x - x * x, 1 to 10)
largesIndex(x => 10 * x - x * x, 1 to 10)